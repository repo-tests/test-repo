<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\UserAddress */

$this->title = 'Изменение адреса пользователя: #' . $model->user->id;
$this->params['breadcrumbs'][] = ['label' => 'Пользователи', 'url' => ['/user/index']];
$this->params['breadcrumbs'][] = ['label' => 'Пользователь #' . $model->user->id, 'url' => ['/user/view', 'id' => $model->user->id]];
$this->params['breadcrumbs'][] = ['label' => 'Адрес #' . $model->id];
$this->params['breadcrumbs'][] = 'Изменение';
?>
<div class="user-address-update">
    <h1><?= Html::encode($this->title) ?></h1>
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>