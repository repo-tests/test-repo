<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\UserAddress */
/* @var $userModel app\models\User */

$this->title = 'Добавление адреса для пользователя #' . $userModel->id;
$this->params['breadcrumbs'][] = ['label' => 'Пользователи', 'url' => ['/user/index']];
$this->params['breadcrumbs'][] = ['label' => 'Адреса пользователя #' . $userModel->id, 'url' => ['/user/view', 'id' => $userModel->id]];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-address-create">
    <h1><?= Html::encode($this->title) ?></h1>
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>