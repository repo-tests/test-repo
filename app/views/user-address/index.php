<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\models\search\UserAddressSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $userID integer */
?>
<div class="user-address-index">
    <h1><?= Html::encode('Адреса пользователя') ?></h1>
    <?php Pjax::begin(); ?>
    <p><?= Html::a('Добавить адрес', ['/user-address/create', 'user_id' => $userID], ['class' => 'btn btn-success']) ?></p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            'id',
            [
                'attribute' => 'address',
                'contentOptions' => ['style'=>'max-width: 200px;']
            ],
            'comment',
            [
                'attribute' => 'created_at',
                'contentOptions' => ['style' => 'width: 160px;']
            ],
            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{delete}',
                'urlCreator' => function($action, $model, $key, $index) {
                    return Yii::$app->urlManager->createUrl(['/user-address/' . $action, 'id' => $key]);
                },
            ],
        ],
    ]); ?>
    <?php Pjax::end(); ?>
</div>