<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use app\models\User;

/* @var $this yii\web\View */
/* @var $model app\models\User */
/* @var $searchModelUserAddress app\models\search\UserAddressSearch */
/* @var $dataProviderUserAddress yii\data\ActiveDataProvider */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Пользователи', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-view">
    <h1><?= Html::encode($this->title) ?></h1>
    <p>
        <?= Html::a('Изменить', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
    </p>
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'name',
            'surname',
            'birthdate:date',
            [
                'attribute' => 'gender',
                'value' => function ($model) {
                    return User::getGenderList()[$model->gender];
                },
            ],
            'created_at',
            'updated_at',
        ],
    ]) ?>
    <?= $this->render('@app/views/user-address/index', [
        'searchModel' => $searchModelUserAddress,
        'dataProvider' => $dataProviderUserAddress,
        'userID' => $model->id,
    ]) ?>
</div>