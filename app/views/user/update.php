<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\User */
/* @var $searchModelUserAddress app\models\search\UserAddressSearch */
/* @var $dataProviderUserAddress yii\data\ActiveDataProvider */

$this->title = 'Изменение пользователя: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Пользователи', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Изменение';
?>
<div class="user-update">
    <h1><?= Html::encode($this->title) ?></h1>
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
    <?= $this->render('@app/views/user-address/index', [
        'searchModel' => $searchModelUserAddress,
        'dataProvider' => $dataProviderUserAddress,
        'userID' => $model->id,
    ]) ?>
</div>