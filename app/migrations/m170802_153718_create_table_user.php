<?php
/**
 * Copyright (c) 8.2017
 * @author Mihail Ivchenko <awrgroup@gmail.com>
 */

use yii\db\Migration;

class m170802_153718_create_table_user extends Migration
{
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%user}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string(255)->notNull(),
            'surname' => $this->string(255)->notNull(),
            'birthdate' => $this->dateTime()->notNull(),
            'gender' => $this->integer(1)->defaultValue(0),
            'status' => $this->smallInteger()->defaultValue(1),
            'created_at' => 'TIMESTAMP DEFAULT CURRENT_TIMESTAMP',
            'updated_at' => 'TIMESTAMP ON UPDATE CURRENT_TIMESTAMP',
        ], $tableOptions);
    }

    public function safeDown()
    {
        $this->dropTable('{{%user}}');
    }
}
