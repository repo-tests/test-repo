<?php
/**
 * Copyright (c) 8.2017
 * @author Mihail Ivchenko <awrgroup@gmail.com>
 */

use yii\db\Migration;

class m170802_154311_create_table_user_address extends Migration
{
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%user_address}}', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer(),
            'address' => $this->string(255),
            'comment' => $this->text(),
            'created_at' => 'TIMESTAMP DEFAULT CURRENT_TIMESTAMP',
            'updated_at' => 'TIMESTAMP ON UPDATE CURRENT_TIMESTAMP',
        ], $tableOptions);

        $this->addForeignKey('fk_user', '{{%user_address}}', 'user_id', '{{%user}}', 'id', 'cascade', 'cascade');
    }

    public function safeDown()
    {
        $this->dropTable('{{%user_address}}');
    }
}
