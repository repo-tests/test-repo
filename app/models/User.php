<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "{{%user}}".
 *
 * @property int $id
 * @property string $name
 * @property string $surname
 * @property string $birthdate
 * @property int $gender
 * @property string $created_at
 * @property string $updated_at
 *
 * @property UserAddress[] $userAddresses
 */
class User extends \yii\db\ActiveRecord
{

    const GENDER_NOT_KNOWN = 0;
    const GENDER_MALE = 1;
    const GENDER_FEMALE = 2;
    const GENDER_NOT_APPLICABLE = 9;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%user}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'surname', 'birthdate'], 'required'],
            ['birthdate', \nsept\birthdaypicker\BirthdayValidator::className()],
            [['birthdate', 'created_at', 'updated_at'], 'safe'],
            [['gender'], 'integer'],
            [['name', 'surname'], 'string', 'max' => 255],
            [['name', 'surname'], 'trim'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Имя',
            'surname' => 'Фамилия',
            'birthdate' => 'Дата рождения',
            'gender' => 'Пол',
            'created_at' => 'Дата создания',
            'updated_at' => 'Дата изменения',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserAddresses()
    {
        return $this->hasMany(UserAddress::className(), ['user_id' => 'id']);
    }

    /**
     * Возвращает массив возможных значений пола для вывода в выпадающем списке
     * @link https://en.wikipedia.org/wiki/ISO/IEC_5218
     * @return array
     */
    public static function getGenderList()
    {
        return array(
            self::GENDER_NOT_KNOWN => 'Не указан',
            self::GENDER_MALE => 'Мужской',
            self::GENDER_FEMALE => 'Женский',
            self::GENDER_NOT_APPLICABLE => 'Неприменимый',
        );
    }
}
